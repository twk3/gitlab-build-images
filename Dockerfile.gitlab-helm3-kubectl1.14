FROM ruby:2.6.5-alpine
MAINTAINER GitLab Engineering Productivity Team

ENV GCLOUD_VERSION=286.0.0
ENV GCLOUD_URL=https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz

ENV HELM_VERSION=3.1.2
ENV HELM_URL=https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
ENV HELM_HOME=/root/.helm

# Install dependencies
RUN apk --no-cache add -U openssl curl tar gzip bash ca-certificates git python2 \
  && mkdir -p /opt

# Install Google Cloud SDK
RUN curl ${GCLOUD_URL} > /tmp/google-cloud-sdk.tar.gz
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

RUN gcloud version

# Install kubectl
RUN gcloud components install kubectl \
  && kubectl version --client

# Install Helm
RUN wget -q -O - ${HELM_URL} | tar zxf - \
  && mv linux-amd64/helm /usr/bin/ \
  && chmod +x /usr/bin/helm \
  && helm version --client

# Install kubeval
RUN mkdir -p $HELM_HOME/plugins \
  && helm plugin install https://github.com/instrumenta/helm-kubeval
